//
//  Hater.swift
//  First
//
//  Created by Elliot Knight on 11/09/2022.
//

import Foundation


struct Hater {
	var hating = false

	mutating func hadABadDay() {
		hating = true
	}

	mutating func hadAGoodDay() {
		hating = false
	}
}
