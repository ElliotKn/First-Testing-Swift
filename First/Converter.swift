//
//  Converter.swift
//  First
//
//  Created by Elliot Knight on 11/09/2022.
//

import Foundation

class Converter {
	func convertToCelsius(fahrenheit: Double) -> Double {
		let fahrenheit = Measurement(value: fahrenheit, unit: UnitTemperature.fahrenheit)
		let celsius = fahrenheit.converted(to: .celsius)
		return celsius.value
 	}
}
