//
//  FirstApp.swift
//  First
//
//  Created by Elliot Knight on 11/09/2022.
//

import SwiftUI

@main
struct FirstApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
